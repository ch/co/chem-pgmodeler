Chemistry's packaging for pgmodeler

```
apt-get build-dep pgmodeler
# download pgmodeler source and extract, cd into it
cd pgmodeler-0.9.1
export QT_SELECT=5
qmake PREFIX=/usr  pgmodeler.pro
make
make INSTALL_ROOT=`pwd`/../ROOT install
cd ..
make build
make upload # etc
```
